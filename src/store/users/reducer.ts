import {IUserAction, IUsersState, UserActionType} from "./types";


const getInitialState = (): IUsersState => ({
    list: [],
    tmpList: [],
    params: { page: 1, results: 50, nat: localStorage.getItem('nat') || '' },
    catalogLoaded: false,
    loading: false
})

export function usersReducer(state=getInitialState(), action: IUserAction): IUsersState {

    switch (action.type) {
        case UserActionType.LOADING:
            return {
                ...state,
                list: [...state.list, ...state.tmpList],
                tmpList: [],
                params: action.params,
                loading: true
            };

        case UserActionType.LOADED:
            const catalogLoaded = state.params.page * state.params.results >= 1000;
            return {
                ...state,
                tmpList: [...action.list],
                catalogLoaded,
                loading: false,
                params: {...state.params, page: state.params.page + 1}
            };

        case UserActionType.SET_NATIONALITY:
            return { ...state, params: { ...state.params, nat: action.nationality }};

        case UserActionType.RESET_STATE:
            return getInitialState();

        default:
            return state
    }
}