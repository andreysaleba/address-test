import {Dispatch} from "redux";
import {IUserAction, IUserRequestParams, UserActionType} from "./types";
import {UserService} from "../../services/users";


export const loadUsers = (params: IUserRequestParams): any => {
    return async (dispatch: Dispatch, getState: Function) => {
        const state = getState();
        if(!state.users.list.length){
            dispatch({
                type: UserActionType.LOADING,
                params,
            });

            const { data } = await UserService.getUsers(
                { ...state.users.params, results: state.users.params.results * 2}
            );
            const tmp = data.results.splice(state.users.params.results -1, state.users.params.results);
            return [
                { type: UserActionType.LOADED, list: data.results },
                { type: UserActionType.LOADING, params },
                { type: UserActionType.LOADED, list: tmp }
            ].forEach((args: IUserAction)=> dispatch(args));

        }

        dispatch({
            type: UserActionType.LOADING,
            params,
        });

        const { data } = await UserService.getUsers(params);

        dispatch({
            type: UserActionType.LOADED,
            list: data.results
        });
    }
};

export const setNationality = (key: string, value: boolean): any => {
    const nat = localStorage.getItem('nat');
    const natArr = nat ? nat.split(',') : [];
    if(value){
        natArr.push(key);
    } else {
        const ind = natArr.findIndex((item: string)=> item === key);
        natArr.splice(ind, 1);
    }
    const nationality = natArr.join(',');
    localStorage.setItem('nat', nationality);
    return (dispatch: Dispatch) => {
        dispatch({
            type: UserActionType.SET_NATIONALITY,
            nationality
        });
    }
};

export const resetUsers = (): any => {
    return (dispatch: Dispatch) => {
        dispatch({
            type: UserActionType.RESET_STATE,
        });
    }
};



