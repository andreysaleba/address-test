import * as React from "react";
import {ChangeEvent} from "react";
import {FormControl, Input, InputAdornment, InputLabel} from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';

interface ISearchProps {
    onQueryChange: (value: string) => void
}

export const Search: React.FC<ISearchProps> = (props: ISearchProps): React.ReactElement => {
    const [value, setValue] = React.useState("");

    const handleChange = (ev: ChangeEvent<HTMLInputElement>) => {
        setValue(ev.target.value);
        props.onQueryChange(ev.target.value);
    };

    return (
        <FormControl fullWidth>
            <InputLabel htmlFor="search">Search</InputLabel>
            <Input
                id="search"
                value={value}
                onChange={handleChange}
                endAdornment={<InputAdornment position="end"><SearchIcon/></InputAdornment>}
            />
        </FormControl>

    )
};