import 'jsdom-global/register';
import * as React from "react";
import UserCard from "./UserCard";
import {mount, shallow} from "enzyme";

const testUser = {
    "gender":"female",
    "name":{
        "title":"ms",
        "first":"juana",
        "last":"prieto"
    },
    "location":{
        "street":"4200 calle de ferraz",
        "city":"lorca",
        "state":"andalucía",
        "postcode": "72997",
        "coordinates":{
            "latitude":"21.8565",
            "longitude":"121.7771"
        },
        "timezone":{
            "offset":"+10:00",
            "description":"Eastern Australia, Guam, Vladivostok"
        }
    },
    "email":"juana.prieto@example.com",
    "login":{
        "uuid":"7bff6770-bd9f-4cba-b2de-fdf38eacaaea",
        "username":"tinywolf862",
        "password":"site",
        "salt":"1L9USNoj",
        "md5":"e41bb883ce42ef2f3eb5f430e98365ac",
        "sha1":"d7997bcd6c1bb35087f1dce845760c1c4278c99a",
        "sha256":"b01f4826ef7fc1a0fa12b91016153544aa75a16dae4192d7bde1df07caed5fc8"
    },
    "dob":{
        "date":"1994-07-04T00:09:22Z",
        "age":24
    },
    "registered":{
        "date":"2005-07-01T19:03:40Z",
        "age":13
    },
    "phone":"986-766-969",
    "cell":"678-242-855",
    "id":{
        "name":"DNI",
        "value":"38929819-J"
    },
    "picture":{
        "large":"https://randomuser.me/api/portraits/women/49.jpg",
        "medium":"https://randomuser.me/api/portraits/med/women/49.jpg",
        "thumbnail":"https://randomuser.me/api/portraits/thumb/women/49.jpg"
    },
    "nat":"ES"
}
    describe('<UserCard/>', () => {
    it('Component should be truthy', () => {
        const search = shallow(<UserCard {...testUser} />);
        expect(search).toBeTruthy();
    });
    it('Fields should be filled correct', () => {

        const comp = mount(<UserCard {...testUser} />);

        expect(comp.props()).toStrictEqual(testUser);
    });

});