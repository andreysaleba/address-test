import * as React from "react";
import {Search} from "../../components/search/Search";
import {AppState} from "../../store";
import {loadUsers, resetUsers} from "../../store/users/actions";
import {Dispatch} from "redux";
import {connect} from "react-redux";
import {IUserRequestParams, User} from "../../store/users/types";
import {useEffect, useMemo} from "react";
import {InfinityScrollGrid} from "../../components/infinityScrollGrid/InfinityScrollGrid";
import UserCard from "../../components/userCard/UserCard";
import {makeStyles, Typography} from "@material-ui/core";

interface IHomePageProps {
    list: User[],
    loading: boolean,
    params: IUserRequestParams,
    loadData: (params: IUserRequestParams) => void,
    resetData: () => void,
    loaded: boolean
}

const useStyles = makeStyles(() => ({
    search: {
        position: 'sticky',
        zIndex: 1,
        top: 0,
        padding: '0 20px 20px',
        backgroundColor: '#fff',
        height: '5vh',
        textAlign: 'right'
    }
}));

const Home: React.FC<IHomePageProps> = (props: IHomePageProps): React.ReactElement => {
    const classes = useStyles({});
    const { list, loading, params, loadData, resetData, loaded } = props;
    const [ usersList, setUsersList ] = React.useState([]);

    const onChange = (query: string) => {
        const filteredList = list.filter(({ name }: User) => {
            return name.first.toLowerCase().includes(query.toLowerCase())
                || name.last.toLowerCase().includes(query.toLowerCase());
        });
        setUsersList(filteredList);
    };

    const onLoad = () => {
        !loaded && loadData(params);
    };

    useEffect(()=> { setUsersList(list) }, [list]);

    useEffect(()=> {
        loadData(params);
        return resetData;
        }, []);

    return (
        <>
            <div className={classes.search}>
                <Search onQueryChange={onChange}/>
            </div>
            {!usersList.length && list.length && !loading ?
                <Typography variant="h6" align="center">Users not found</Typography>
                : <InfinityScrollGrid
                    loading={loading}
                    loaded={loaded}
                    component={UserCard}
                    list={usersList}
                    listEndComponent={()=> <Typography variant="h6" align="center">End of users catalog</Typography>}
                    onLoad={onLoad}></InfinityScrollGrid>}
        </>
    )
};

const mapStateToProps = (store: AppState) => ({
    list: store.users.list,
    loading: store.users.loading,
    params: store.users.params,
    loaded: store.users.catalogLoaded
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
    loadData: (params: IUserRequestParams) => {
        dispatch(loadUsers(params));
    },
    resetData: () => {
        dispatch(resetUsers());
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Home)